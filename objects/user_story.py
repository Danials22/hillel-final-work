#!/usr/bin/env python
# coding: utf-8
from .exchanger import Exchanger
from utils import helpers


class UserStory:
    def __init__(self):
        self.exchanger = Exchanger()


    def start(self):
        while True:
            command_list = input("COMMAND?\n").strip().upper().split(" ")
            command = command_list[0]

            match command:
                case 'COURSE':
                    if not helpers.is_valid_command(command_list):
                        continue

                    currency = command_list[1]
                    if not helpers.is_valid_currency(currency):
                        continue

                    course = self.exchanger.get_current_course(currency)
                    if not helpers.is_valid_course(course):
                        continue

                    limit = self.exchanger.get_current_limit(currency)
                    print(f"RATE {course}, AVAILABLE {limit}")

                case 'EXCHANGER':
                    if not helpers.is_valid_command(command_list, limit=2):
                        continue

                    currency = command_list[1]
                    if not helpers.is_valid_currency(currency):
                        continue

                    currency_value = command_list[2]
                    if not helpers.is_float(currency_value):
                        continue

                case 'STOP':
                    print("SERVICE STOPPED")
                    break

                case _:
                    print("INVALID COMMAND")