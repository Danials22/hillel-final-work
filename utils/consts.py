#!/usr/bin/env python
# coding: utf-8

CURRENCIES = {
    "base": "USD",
    "to": "UAH"
}

FILE_DATA = {
    "USD": 2000.0,
    "UAH": 20000.0
}
