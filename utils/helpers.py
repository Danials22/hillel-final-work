#!/usr/bin/env python
# coding: utf-8
from typing import List
from .consts import CURRENCIES


def is_valid_command(command_list: List, limit: int = 1) -> bool:
    if len(command_list) <= limit:
        print("INVALID COMMAND")
        return False
    return True


def is_valid_currency(currency: str) -> bool:
    if currency not in list(CURRENCIES.values()):
        print(f"INVALID CURRENCY{currency}")
        return False
    return True


def is_valid_course(course: float) -> bool:
    if course == 0.0:
        print("API ERROR")
        return False
    return True


def is_float(params: str) -> bool:
    try:
        float(params)
        return True
    except ValueError:
        print("CURRENCY VALUE ERROR")
        return False

